package com.mongo.repository;

import com.mongo.domain.Email;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Sakshi on 3/6/17.
 */
@Repository
public interface EmailRepository extends MongoRepository<Email,String> {
}
